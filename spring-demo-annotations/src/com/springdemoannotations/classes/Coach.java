package com.springdemoannotations.classes;

public interface Coach {

	public String getDailyWorkout();
	
	
	public String getDailyFortune();
	
}
