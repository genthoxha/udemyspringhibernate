package com.springdemoannotations.classes;

public interface FortuneService {

	public String getFortune();
	
}
