package com.springudemy.aopdemo;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.springudemy.aopdemo.dao.AccountDAO;

public class AfterReturningDemoApp {
	
	public static void main(String[] args) {
		
		// read the spring configuration java class
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);

		// get the bean from spring container
		AccountDAO accountDAO = context.getBean("accountDAO",AccountDAO.class);
		
		// call the methods to find the account
		List<Account> theAccounts = accountDAO.findAccounts();
		
		// display the accounts 
		System.out.println("---------------------------------------------------------------------------------");
		System.out.println("\n\nMain Program: AfterReturningDemoApp");
		
		System.out.println(theAccounts);
		
		System.out.println("\n");
		
		// close the context
		context.close();
	}

}
