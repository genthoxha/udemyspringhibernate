package com.springudemy.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class UtilityLuvAopExpressions {

	@Pointcut("execution(* com.springudemy.aopdemo.dao.*.*(..))")
	public void forDaoPackage() {
	}

	// create point cut for getter methods
	@Pointcut("execution(* com.springudemy.aopdemo.dao.*.get*(..))")
	public void getter() {
	}

	// create point cut for setter methods
	@Pointcut("execution(* com.springudemy.aopdemo.dao.*.set*(..))")
	public void setter() {
	}

	// create point: include package ... exlude getter/setter
	@Pointcut("forDaoPackage() && !( getter() || setter())")
	public void forDaoPackageNoGetterSetter() {
	}

}
