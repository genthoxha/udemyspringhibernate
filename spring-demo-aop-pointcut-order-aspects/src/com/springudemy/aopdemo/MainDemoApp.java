package com.springudemy.aopdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.springudemy.aopdemo.dao.AccountDAO;
import com.springudemy.aopdemo.dao.MembershipDAO;

public class MainDemoApp {
	
	public static void main(String[] args) {
		
		// read spring config java class
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
		
		// get the bean from spring container
		AccountDAO theAccountDAO = context.getBean("accountDAO",AccountDAO.class);
		
		// get membership bean from spring
		MembershipDAO theMembershipDAO = context.getBean("membershipDAO",MembershipDAO.class);
		
		// call the business method
		Account a = new Account();
		theAccountDAO.addAccount(a,true);
		theAccountDAO.doWork();

		// call the accountdao getter/setter methods
		
		theAccountDAO.setName("foobar");
		theAccountDAO.setServiceCode("silver");
		
		String name = theAccountDAO.getName();
		String code = theAccountDAO.getServiceCode();
		
		
		System.out.println("---------------------------------------------------------------------------------------------");
		
		// call the business method again 
		theMembershipDAO.addAccount();
		theMembershipDAO.goToSleep();
		
		// close the context
		context.close();
		
	}

}
