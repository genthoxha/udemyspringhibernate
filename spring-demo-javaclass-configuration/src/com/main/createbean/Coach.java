package com.main.createbean;

public interface Coach {

	public String getWorkout();
	
	public String getDailyFortune();
	
}
