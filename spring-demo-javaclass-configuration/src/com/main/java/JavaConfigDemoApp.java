package com.main.java;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JavaConfigDemoApp {
	
	public static void main(String[] args) {
		
		// read spring config java class
		
		AnnotationConfigApplicationContext annotationConfigApplicationContext = 
				new AnnotationConfigApplicationContext(SportConfig.class); 
		// get the bean from spring container
		
		Coach theCoach = annotationConfigApplicationContext.getBean("tennisCoach",Coach.class);
		
		
		// call the method on bean
		System.out.println(theCoach.dailyExercise());
		
		// close the context
		annotationConfigApplicationContext.close();
		
	}

}
