package com.main.java;

import org.springframework.stereotype.Component;

@Component
public class TennisCoach implements Coach {

	@Override
	public String dailyExercise() {
		return "Daily exercise has started!";
	}

}
