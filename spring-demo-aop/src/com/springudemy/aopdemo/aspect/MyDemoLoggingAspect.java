package com.springudemy.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyDemoLoggingAspect {

	
	// this is where we add all of our related advices for logging
	
	// let's start with an @Before advice
	
//	@Before("execution(public void addAccount())")
	
//	@Before("execution(public void com.springudemy.aopdemo.dao.AccountDAO.addAccount())")
	
	// modifier public return type of void
//	@Before("execution(public void add*())")
	
	// return type of void 
//	@Before("execution(void add*())")
	
	// any return type WITH 'ADD'
//	@Before("execution(*add*())")
	
//	@Before("execution(* add*(com.springudemy.aopdemo.Account, ..))")

	@Before("execution(* com.springudemy.aopdemo.dao.*.*(..))")
	public void beforeAddAccountAdvice() {
		
		System.out.println("\n======>>> Executing @Before advice on addAccount()");
		
	}
	
	
	
	
	
	
	
}
