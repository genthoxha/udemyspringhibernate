package com.springudemy.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyDemoLoggingAspect {

	
	@Pointcut("execution(* com.springudemy.aopdemo.dao.*.*(..))")
	private void forDaoPackage() {}

	// create point cut for getter methods
	@Pointcut("execution(* com.springudemy.aopdemo.dao.*.get*(..))")
	private void getter() {}
	
	
	// create point cut for setter methods
	@Pointcut("execution(* com.springudemy.aopdemo.dao.*.set*(..))")
	private void setter() {}
	
	// create point: include package ... exlude getter/setter
	@Pointcut("forDaoPackage() && !( getter() || setter())")
	private void forDaoPackageNoGetterSetter() {}
	
	
	@Before("forDaoPackageNoGetterSetter()")
	public void beforeAddAccountAdvice() {
		
		System.out.println("\n======>>> Executing @Before advice on addAccount()");
		
	}
	
	@Before("forDaoPackageNoGetterSetter()")
	public void performApiAnalytics() {
		System.out.println("\n======> Performing API analytics");
	}
	
	@Before("forDaoPackageNoGetterSetter()")
	public void logToCloudAsync() {
		System.out.println("\n======> Logging to Clound in async fashion");
	}
	
	
	
	
	
	
	
}
