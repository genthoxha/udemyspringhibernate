package com.classes;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext  context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		People p1 = context.getBean("goodPeople",People.class);
		
		People p2 = context.getBean("badPeople",People.class);
		
		System.out.println("-------------------------------------------------------------------------------");
		System.out.println(p1.getClass().getSimpleName() + " "+ p1.behaviour());
		System.out.println("-------------------------------------------------------------------------------");
		System.out.println(p2.getClass().getSimpleName() + " "+p2.behaviour());
		
		System.out.println("-------------------------------------------------------------------------------");
		System.out.println(p1.getClass().getSimpleName() + " "+ p1.live());
		System.out.println("-------------------------------------------------------------------------------");
		System.out.println(p2.getClass().getSimpleName() + " "+p2.live());
		
		
		
	}
	
}
