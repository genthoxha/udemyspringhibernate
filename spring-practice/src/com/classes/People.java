package com.classes;

public interface People {

	public void breath();
	
	public void eat();
	
	public String behaviour();
	
	public String live();
	
}
