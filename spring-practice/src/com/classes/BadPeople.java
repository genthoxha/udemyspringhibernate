package com.classes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BadPeople implements People {
	
	public BadPeople() {
		
	}
	private Live live;
	@Override
	public void breath() {
		System.out.println("Bad people breath , so that means they can be also good people !");
	}

	@Override
	public void eat() {
		System.out.println("Yes food is neccessary for anyone !");
	}

	@Override
	public String behaviour() {
		return "Bad habits !";
	}
	

	@Autowired
	public void setLiveMethod(Live l) {
		live = l;
	}
	
	public String live() {
		return live.getLiveMethod();
	}
	
}
