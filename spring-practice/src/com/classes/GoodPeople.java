package com.classes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GoodPeople implements People {
	
	private Live live;

	public GoodPeople() {
		
	}
	@Override
	public void breath() {
		System.out.println("Good people breath , so that means they can be also bad people !");
	}

	@Override
	public void eat() {
		System.out.println("Yes food is neccessary for anyone !");
	}

	@Override
	public String behaviour() {
		return "Good habits !";
	}

	@Autowired
	public void setLiveMethod(Live l) {
		live = l;
	}

	@Override
	public String live() {
		return live.getLiveMethod();
	}
	
	
	
	
}
