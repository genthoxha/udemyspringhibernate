package com.springudemy.aopdemo.aspect;

import java.util.List;
import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.springudemy.aopdemo.Account;

@Aspect
@Component
@Order(2)
public class MyDemoLoggingAspect {
	
	private Logger myLogger = Logger.getLogger(getClass().getName());
	
	@Around("execution(* com.springudemy.aopdemo.service.*.getFortune(..))")	
	public Object aroundGetFortune(
			ProceedingJoinPoint theProceedingJoinPoint)throws Throwable {
		
		// print out method we are advising on 
				String method = theProceedingJoinPoint.getSignature().toShortString();
				myLogger.info("\n=====> Executing @After  on method: " + method);
				
		// get begin timestamp
		long begin = System.currentTimeMillis();
		myLogger.info("Begin time: "+begin/1000.0 );
		
		// now, let's execute the method
		Object result = null;
		
		try {
			result = theProceedingJoinPoint.proceed();
		}catch(Exception e) {
			
			// log the exception
			myLogger.warning(e.getMessage());
/*			
			// give user a custom message
			result = "Major accident! But no worries your private AOP Helicopter is on the way!";*/
			
			// rethrow exception
			
			throw e;
			
		}
		
		// get end timestamp
		long end = System.currentTimeMillis();
		myLogger.info("End time: "+end/1000.0 );
		
		// compute duration and display it
		long duration = end - begin;
		
		myLogger.info("\n DURATION: "+duration/1000.0 + " seconds");
		
		return result;
	}
	
	
	@After("execution(* com.springudemy.aopdemo.dao.AccountDAO.findAccounts(..))")
	public void afterFinallyFindAccountsAdvice(JoinPoint theJoinPoint) {
		
		// print out which method we are advising on
		String method = theJoinPoint.getSignature().toShortString();
		System.out.println("\n=====> Executing @After ( Finally )  on method: " + method);
		
	}
	
	

	@AfterThrowing(pointcut = "execution(* com.springudemy.aopdemo.dao.AccountDAO.findAccounts(..))", throwing = "theExc")
	public void afterThrowingFindAccountAdvice(JoinPoint theJoinPoint, Throwable theExc) {

		// print out which method we are advising on
		String method = theJoinPoint.getSignature().toShortString();
		System.out.println("\n=====> Executing @AfterReturning on method: " + method);
		
		// log the exception
		System.out.println("\n=====> EXCEPTION :  is: " + theExc);

	}

	// add a new advice for @AfterReturning on the findAccounts method
	@AfterReturning(pointcut = " execution(* com.springudemy.aopdemo.dao.AccountDAO.findAccounts(..))", returning = "result")
	public void afterReturningFindAccountsAdvice(JoinPoint theJoinPoint, List<Account> result) {

		// print out which method we are advising on
		String method = theJoinPoint.getSignature().toShortString();
		System.out.println("\n=====> Executing @AfterReturning on method: " + method);

		// print out the results of the method call
		System.out.println("\n=====> Result is: " + result);

		// let's post-process the data .. let's modify it :-)

		// convert the account names to uppercase
		convertTheAccountNamesToUpperCase(result);

		// print out the results after modified of the method call
		System.out.println("\n=====> Result after modified is: " + result);

	}

	private void convertTheAccountNamesToUpperCase(List<Account> result) {

		// loop through accounts
		for (Account tempAccount : result) {

			// get uppercase version of name
			String theUpperName = tempAccount.getName().toUpperCase();
			// update the name on the account
			tempAccount.setName(theUpperName);

		}

	}

	@Before("com.springudemy.aopdemo.aspect.UtilityLuvAopExpressions.forDaoPackageNoGetterSetter()")
	public void beforeAddAccountAdvice(JoinPoint theJoinPoint) {

		System.out.println("\n======>>> Executing @Before advice on addAccount()");

		// display the method signature
		MethodSignature methodSig = (MethodSignature) theJoinPoint.getSignature();

		System.out.println("Method: " + methodSig);

		// get the arguments
		Object[] args = theJoinPoint.getArgs();

		// display method arguments
		for (Object tempArg : args) {
			System.out.println(tempArg);

			if (tempArg instanceof Account) {

				// downcast and print Account specific stuff
				Account theAccount = (Account) tempArg;

				System.out.println("account name: " + theAccount.getName());
				System.out.println("account level: " + theAccount.getLevel());

			}
		}

	}

}
