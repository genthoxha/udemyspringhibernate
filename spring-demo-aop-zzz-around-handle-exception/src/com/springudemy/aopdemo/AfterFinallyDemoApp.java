package com.springudemy.aopdemo;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.springudemy.aopdemo.dao.AccountDAO;

public class AfterFinallyDemoApp {
	
	public static void main(String[] args) {
		
		// read the spring configuration java class
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);

		// get the bean from spring container
		AccountDAO accountDAO = context.getBean("accountDAO",AccountDAO.class);
		
		// call the methods to find the account
		List<Account> theAccounts = null;
		System.out.println("-------------------EXCEPTION--------------------------------------------------------------");
		try {
			// add a boolean flag to simulate exceptions
			boolean tripWire = false;
			theAccounts = accountDAO.findAccounts(tripWire);
			
		}catch(Exception exc) {
			System.out.println("\n\nMain program ... caught exception:  "+exc);
		}
		System.out.println("-------------------END EXCEPTION--------------------------------------------------------------");
		// display the accounts 
		System.out.println("---------------------------------------------------------------------------------");
		System.out.println("\n\nMain Program: AfterThrowingDemoApp");
		
		System.out.println("\n");
		
		// close the context
		context.close();
	}

}
