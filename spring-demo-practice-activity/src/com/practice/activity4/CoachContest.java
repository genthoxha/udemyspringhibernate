package com.practice.activity4;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CoachContest {
	public static void main(String[] args) {
		
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("four-applicationContext.xml");
		
		
		BasketballCoach coach = context.getBean("basketballCoach",BasketballCoach.class);
		
		System.out.println(coach.swim());
		context.close();
	}
}
