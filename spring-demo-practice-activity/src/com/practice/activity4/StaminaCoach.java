package com.practice.activity4;

public interface StaminaCoach {

	public void run();
	
	public String swim();
}
