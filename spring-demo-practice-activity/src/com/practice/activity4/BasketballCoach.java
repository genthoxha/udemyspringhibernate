package com.practice.activity4;

import org.springframework.stereotype.Component;

@Component
public class BasketballCoach implements StaminaCoach {
	
	public BasketballCoach() {
		System.out.println(">> BasketBallCoach: inside default constructor");
	}

	@Override
	public void run() {
		System.out.println("You should run every single day for keeping your stamina on right level !");
	}

	@Override
	public String swim() {
		return "Don't forget to swim twice a week";
	}

	
	
}
