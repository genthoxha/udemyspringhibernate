package com.practice.activity5;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("five-applicationContext.xml");
		
		Coach coacher = context.getBean("happyFortuneService",Coach.class);
		
		System.out.println(coacher.getDailyFortune());
	}
}
