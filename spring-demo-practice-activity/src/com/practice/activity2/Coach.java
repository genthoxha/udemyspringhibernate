package com.practice.activity2;



public interface Coach {

	public String getDailyWorkout();
	
	public String getDailyFortune();
	
}
